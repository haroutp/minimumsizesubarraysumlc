﻿using System;

namespace MaximumSizeSubarraySum
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(MinSubArrayLen(7, new int[]{2,3,1,2,4,3}));
        }

        public static int MinSubArrayLen(int s, int[] nums){
            
            int SubarraySize = int.MaxValue;

            int left = 0;

            int val = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                val += nums[i];

                while(val >= s){
                    SubarraySize = Math.Min(SubarraySize, i + 1 - left);
                    val -= nums[left];
                    left++;
                }
            }

            return (SubarraySize != int.MaxValue)? SubarraySize : 0;
            
            
        }
    }
}
